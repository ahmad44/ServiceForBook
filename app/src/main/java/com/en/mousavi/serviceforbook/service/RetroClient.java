package com.en.mousavi.serviceforbook.service;

import com.en.mousavi.serviceforbook.AppConst;
import com.en.mousavi.serviceforbook.model.BookService;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Ahmad on 11/15/2017.
 */

public interface RetroClient {

    @GET(AppConst.BOOKSERVICE)
    Observable<BookService> bookService();


}
