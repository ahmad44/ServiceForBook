
package com.en.mousavi.serviceforbook.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class Book extends SugarRecord {

    @SerializedName("book_id")
    @Expose
    private Integer idBook;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("type_price")
    @Expose
    private String typePrice;

    @SerializedName("category")
    @Expose
    private String category;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("author")
    @Expose
    private String author;

    @SerializedName("rate")
    @Expose
    private Double rate;

    @SerializedName("comment")
    @Expose
    private Integer comment;

    @SerializedName("page")
    @Expose
    private Integer page;

    @SerializedName("price")
    @Expose
    private Integer price;

    public Book() {

    }

    public Integer getIdBook() {
        return idBook;
    }

    public void setId(Integer id) {
        this.idBook = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypePrice() {
        return typePrice;
    }

    public void setTypePrice(String typePrice) {
        this.typePrice = typePrice;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Integer getComment() {
        return comment;
    }

    public void setComment(Integer comment) {
        this.comment = comment;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "idBook=" + idBook +
                ", name='" + name + '\'' +
                ", typePrice='" + typePrice + '\'' +
                ", category='" + category + '\'' +
                ", image='" + image + '\'' +
                ", description='" + description + '\'' +
                ", author='" + author + '\'' +
                ", rate=" + rate +
                ", comment=" + comment +
                ", page=" + page +
                ", price=" + price +
                '}';
    }
}
