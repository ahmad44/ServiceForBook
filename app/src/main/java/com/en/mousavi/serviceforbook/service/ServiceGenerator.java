package com.en.mousavi.serviceforbook.service;

/**
 * Created by hosseini on 11/15/2017.
 */


import com.en.mousavi.serviceforbook.AppConst;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by barakat_4065 on 2016-11-05.
 */

public class ServiceGenerator {
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(AppConst.URL)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create());


    public static <S> S createService(Class<S> serviceClass) {
        Interceptor header = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Accept", "application/x.api.v1+json");

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        };

        HttpLoggingInterceptor log = new HttpLoggingInterceptor();

        log.setLevel(HttpLoggingInterceptor.Level.BODY);


        Retrofit retrofit = builder.client(httpClient
                .addInterceptor(header)
                .addInterceptor(log)
                .build())
                .build();

        return retrofit.create(serviceClass);
    }


}
