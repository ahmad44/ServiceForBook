package com.en.mousavi.serviceforbook;

import android.app.Application;

import com.orm.SugarContext;

/**
 * Created by hosseini on 11/15/2017.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }
}
