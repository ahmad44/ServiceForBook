package com.en.mousavi.serviceforbook.service;

import com.en.mousavi.serviceforbook.AppConst;
import com.en.mousavi.serviceforbook.model.BookService;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by hosseini on 11/15/2017.
 */

public interface RetroClientCall {

//
//    @GET(AppConst.BOOKSERVICE)
//    Call<BookService> bookService();



    @GET(AppConst.BOOKSERVICE)
    Call<JsonObject> bookService();
}
