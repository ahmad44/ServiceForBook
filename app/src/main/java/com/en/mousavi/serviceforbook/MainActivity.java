package com.en.mousavi.serviceforbook;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.en.mousavi.serviceforbook.model.Book;
import com.en.mousavi.serviceforbook.model.BookService;
import com.en.mousavi.serviceforbook.service.RetroClient;
import com.en.mousavi.serviceforbook.service.RetroClientCall;
import com.en.mousavi.serviceforbook.service.ServiceGenerator;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    RetroClient retroClient;
    private Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //testServiceWithRxJava();
        testServiceWithCall();
    }

    private void testServiceWithCall() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConst.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetroClientCall retroClient = retrofit.create(RetroClientCall.class);
        Call<JsonObject> call = retroClient.bookService();

        Callback<JsonObject> callback = new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {


                        Log.i("Book ", "Book: " + response.body().get("status"));


//                    for (Book book : response.body().getBooks()) {
//                        Log.i("Book ", "Book: " + book.getName() + " Author: " + book.getAuthor());
//                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i("Book ", " onFailure: ");
            }
        };

        call.enqueue(callback);
    }

    private void testServiceWithRxJava() {
        try {

            retroClient = ServiceGenerator.createService(RetroClient.class);
            subscription = retroClient
                    .bookService()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<BookService>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(BookService bookService) {
                            for (Book book : bookService.getBooks()) {
                                book.save();
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void use() {
        List<Book> books = Book.listAll(Book.class);
        Book book = Book.findById(Book.class, 1);

    }
    }
